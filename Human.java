import java.text.DateFormat;
import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private DayOfWeek[][] schedule;

    private Pet pet;
    private Family family;
    static {
        System.out.println("Family class is being loaded...");
    }

    {
        System.out.println("A new Family object is created...");
    }

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, int iq, DayOfWeek[][] schedule, Pet pet) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
        this.pet = pet;
    }

    public Human() {

    }

    public DayOfWeek[][] getSchedule() {
        return schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public void setSchedule(DayOfWeek[][] schedule) {
        this.schedule = schedule;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public void set_name(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void set_surname(String surname) {
        this.surname = surname;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public void greetPet() {
        System.out.printf("Hello,%s%n", pet.getNickname());
    }

    public void describePet() {
        String s = (pet.getTrickLevel() >= 50) ? "very sly" : "almost not sly";
        System.out.printf("I have an %s is %d years old, he is %s%n", pet.getSpecies(), pet.getAge(), s);
    }

    @Override
    public String toString() {
        return "Human{name=%s, surname=%s, year=%d, iq=%d, schedule=%s}".formatted(name, surname, year, iq, Arrays.deepToString(schedule));
    }

    public boolean feedPet(boolean time) {
        if (time || new Random().nextInt(101) < pet.getTrickLevel()) {
            System.out.println("Hm... I will feed " + pet.getNickname());
            return true;
        } else {
            System.out.println("I think Jack is not hungry.");
            return false;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return year == human.year && iq == human.iq && Objects.equals(name, human.name) && Objects.equals(surname, human.surname) && Arrays.equals(schedule, human.schedule) && Objects.equals(pet, human.pet) && Objects.equals(family, human.family);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name, surname, year, iq, pet, family);
        result = 31 * result + Arrays.hashCode(schedule);
        return result;
    }

}
