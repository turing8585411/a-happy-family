import java.util.Arrays;
import java.util.Objects;

public abstract class Pet {
    private Species _species;
    private String _nickname;
    private int _age;
    private int _trickLevel;
    private String[] habits;
    static {
        System.out.println("Family class is being loaded...");
    }

    {
        System.out.println("A new Family object is created...");
    }

    public Pet(Species species, String nickname) {
        this._species = species;
        this._nickname = nickname;
    }

    public Pet(Species species, String nickname, int age, int trickLevel, String[] habits) {
        this._species = species;
        this._nickname = nickname;
        this._age = age;
        this._trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pet() {

    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public int getTrickLevel() {
        return _trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this._trickLevel = trickLevel;
    }

    public int getAge() {
        return _age;
    }

    public void setAge(int age) {
        this._age = age;
    }

    public Species getSpecies() {
        return _species;
    }

    public void setSpecies(Species species) {
        this._species = species;
    }

    public String getNickname() {
        return _nickname;
    }

    public void setNickname(String nickname) {
        this._nickname = nickname;
    }

    public void eat() {
        System.out.println("I am eating");
    }

    public void respond() {
        System.out.printf("Hello, owner. I am %s. I miss you!%n", _nickname);
    }

    public void foul() {
        System.out.println("I need to cover it up.");
    }

    @Override
    public String toString() {
        return "%s{nickname=%s, age=%d, trickLevel=%d, habits=%s}".formatted(getSpecies(), getNickname(), getAge(), getTrickLevel(), Arrays.toString(habits));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return _age == pet._age && _trickLevel == pet._trickLevel && _species == pet._species && Objects.equals(_nickname, pet._nickname) && Arrays.equals(habits, pet.habits);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(_species, _nickname, _age, _trickLevel);
        result = 31 * result + Arrays.hashCode(habits);
        return result;
    }
}
