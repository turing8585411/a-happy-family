import java.util.Arrays;
import java.util.Objects;

public class Family {
    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;
    int numberOfChildren;

    static {
        System.out.println("Family class is being loaded...");
    }

    {
        System.out.println("A new Family object is created...");
    }

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.children = new Human[0];
        this.pet = getPet();
        this.numberOfChildren = 0;
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {

        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    @Override
    public String toString() {
        return " mother=%s father=%s children= %s number of children=%d, count family=%d pet=%s".formatted(getMother(), getFather(), Arrays.toString(getChildren()), numberOfChildren, countFamily(), getPet());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return numberOfChildren == family.numberOfChildren && Objects.equals(mother, family.mother) && Objects.equals(father, family.father) && Arrays.equals(children, family.children) && Objects.equals(pet, family.pet);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father, pet, numberOfChildren);
        result = 31 * result + Arrays.hashCode(children);
        return result;
    }

    public boolean deleteChild(Human child) {
        for (int i = 0; i < children.length; i++) {
            if (children[i].equals(child)) {
                // Found the child to remove
                Human[] newChildren = new Human[children.length - 1];
                int newIndex = 0;
                for (int j = 0; j < children.length; j++) {
                    if (j != i) {
                        newChildren[newIndex] = children[j];
                        newIndex++;
                    }
                }
                children = newChildren;
                numberOfChildren--;
                child.setFamily(null);
                return true;
            }
        }
        return false;
    }


    public int countFamily() {
        return 2 + numberOfChildren;
    }

    public void addChild(Human child) {
        Human[] newChildren = Arrays.copyOf(children, children.length + 1);
        newChildren[children.length] = child;
        children = newChildren;
        numberOfChildren++;
        child.setFamily(this);
    }
}
