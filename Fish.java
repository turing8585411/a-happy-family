public class Fish extends Pet {
    public Fish(String nickname, int age) {
        super(Species.FISH, nickname, age, 0, new String[]{"swim"});
    }

    @Override
    public Species getSpecies() {
        return Species.FISH;
    }

    @Override
    public void respond() {
        System.out.println("...");
    }
}
