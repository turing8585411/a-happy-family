public class Main {
    public static void main(String[] args) {

        Pet pet1 = new Dog("Buddy", 3, 80, new String[]{"eat", "run", "sleep"});
        //Pet pet2 = new Cat(Species.CAT, "Whiskers", 2, 90, new String[]{"meow", "purr", "nap"});


        DayOfWeek[][] schedule1 = {
                {DayOfWeek.MONDAY, DayOfWeek.TUESDAY},
                {DayOfWeek.WEDNESDAY, DayOfWeek.THURSDAY},
                {DayOfWeek.FRIDAY, DayOfWeek.SATURDAY},
                {DayOfWeek.SUNDAY}
        };

        Human mother = new Human("Ayan", "Haciyeva", 2002, 23, schedule1, pet1);
        Human father = new Human("Ferid", "Eliyev", 2000);

        Human child1 = new Human("Aylin", "Eliyeva", 2004);
        child1.setPet(pet1);
        Human child2 = new Human("Feride", "Eliyeva", 2004);
//        child2.setPet(pet2);


        child1.greetPet();
        child1.describePet();
        System.out.println(child2);
        boolean fed = child1.feedPet(true);
        System.out.println(fed);

        Family family1 = new Family(mother, father);

//        family1.addChild(child1);
        family1.addChild(child2);
        //   family1.deleteChild(child2);
        System.out.println(family1);


    }
}
